# Импортируем библиотеки random и tkinder
from tkinter import *
import random

# Ширина и высота игрового поля
WIDTH = 750
HEIGHT = 350

# Игровые ракетки (ширина и высота)
PAD_W = 10
PAD_H = 125

# Игровой шар (Радиус плюс настройки скорости)
BALL_RADIUS = 30
BALL_SPEED_UP = 1.05
BALL_MAX_SPEED = 30

INITIAL_SPEED = 10
BALL_X_SPEED = INITIAL_SPEED
BALL_Y_SPEED = INITIAL_SPEED

# Итоговый счет (компьютер & игрок)
COMPUTER_SCORE = 0
PLAYER_SCORE = 0

# Добавим глобальную переменную отвечающую за расстояние
# до правого края игрового поля
right_line_distance = WIDTH - PAD_W

def update_score(player):
    global COMPUTER_SCORE, PLAYER_SCORE
    if player == "right":
        COMPUTER_SCORE += 1
        canvas.itemconfig(p_1_text, text=COMPUTER_SCORE)
    else:
        PLAYER_SCORE += 1
        canvas.itemconfig(p_2_text, text=PLAYER_SCORE)

def winner(player):
    if player == "right":
        canvas.itemconfig(p_1_text, text="WIN!!!")
    else:
        canvas.itemconfig(p_2_text, text="WIN!!!")

def spawn_ball():
    global BALL_X_SPEED
    # Центрируем мяч
    canvas.coords(BALL, WIDTH / 2 - BALL_RADIUS / 2,
                  HEIGHT / 2 - BALL_RADIUS / 2,
                  WIDTH / 2 + BALL_RADIUS / 2,
                  HEIGHT / 2 + BALL_RADIUS / 2)
    # Каждый новый старт идет в сторону проигравшего
    BALL_X_SPEED = -(BALL_X_SPEED * -INITIAL_SPEED) / abs(BALL_X_SPEED)


# Функция для контроля отскока мяча
def bounce(action):
    global BALL_X_SPEED, BALL_Y_SPEED
    if action == "strike":
        BALL_Y_SPEED = random.randrange(-10, 10)
        if abs(BALL_X_SPEED) < BALL_MAX_SPEED:
            BALL_X_SPEED *= -BALL_SPEED_UP
        else:
            BALL_X_SPEED = -BALL_X_SPEED
    else:
        BALL_Y_SPEED = -BALL_Y_SPEED


# Создаем игровое поле
root = Tk()
root.title("Мини-игра - Ping-Pong")

canvas = Canvas(root, width=WIDTH, height=HEIGHT, background="#4682B4")
canvas.pack()

# границы поля
canvas.create_line(PAD_W, 0, PAD_W, HEIGHT, fill="white")
canvas.create_line(WIDTH - PAD_W, 0, WIDTH - PAD_W, HEIGHT, fill="white")
# Линии разграничения поля
canvas.create_line(WIDTH / 2, 0, WIDTH / 2, HEIGHT, fill="white")
canvas.create_line(PAD_W, HEIGHT / 2, WIDTH - PAD_W, HEIGHT / 2, fill="white", dash=(4,2))

# Создание игрового мяча
BALL = canvas.create_oval(WIDTH / 2 - BALL_RADIUS / 2, HEIGHT / 2 - BALL_RADIUS / 2,
                          WIDTH / 2 + BALL_RADIUS / 2, HEIGHT / 2 + BALL_RADIUS / 2, fill="#F5DEB3")
# Ракетки
LEFT_PAD = canvas.create_line(PAD_W / 2, 0, PAD_W / 2, PAD_H, width=PAD_W, fill="#DAA520")
RIGHT_PAD = canvas.create_line(WIDTH - PAD_W / 2, 0, WIDTH - PAD_W / 2, PAD_H, width=PAD_W, fill="#DAA520")

p_1_text = canvas.create_text(WIDTH - WIDTH / 6, PAD_H / 4,
                              text=COMPUTER_SCORE,
                              font="Arial 20",
                              fill="white")

p_2_text = canvas.create_text(WIDTH / 6, PAD_H / 4,
                              text=PLAYER_SCORE,
                              font="Arial 20",
                              fill="white")

# Переменные для скорости движения мяча
BALL_X_CHANGE = 20
BALL_Y_CHANGE = 0
# Максимальный счет до которого длится игра
MAXIMUM_SCORE = 5

def move_ball():
    # определяем координаты сторон мяча и его центра
    ball_left, ball_top, ball_right, ball_bot = canvas.coords(BALL)
    ball_center = (ball_top + ball_bot) / 2

    # вертикальный отскок
    # Если мы далеко от вертикальных линий - просто двигаем мяч
    if ball_right + BALL_X_SPEED < right_line_distance and \
            ball_left + BALL_X_SPEED > PAD_W:
        canvas.move(BALL, BALL_X_SPEED, BALL_Y_SPEED)
    # Если мяч касается своей правой или левой стороной границы поля
    elif ball_right == right_line_distance or ball_left == PAD_W:
        # Проверяем правой или левой стороны мы касаемся
        if ball_right > WIDTH / 2:
            # Если правой, то сравниваем позицию центра мяча с позицией правой ракетки.
            # И если мяч в пределах ракетки делаем отскок
            if canvas.coords(RIGHT_PAD)[1] < ball_center < canvas.coords(RIGHT_PAD)[3]:
                bounce("strike")
            else:
                update_score("left")
                if PLAYER_SCORE < MAXIMUM_SCORE:
                    spawn_ball()
                else:
                    winner("left")
        else:
            if canvas.coords(LEFT_PAD)[1] < ball_center < canvas.coords(LEFT_PAD)[3]:
                bounce("strike")
            else:
                update_score("right")
                if COMPUTER_SCORE < MAXIMUM_SCORE:
                    spawn_ball()
                else:
                    winner("right")
    # Проверка ситуации, в которой мячик может вылететь за границы игрового поля.
    # В таком случае просто двигаем его к границе поля.
    else:
        if ball_right > WIDTH / 2:
            canvas.move(BALL, right_line_distance - ball_right, BALL_Y_SPEED)
        else:
            canvas.move(BALL, -ball_left + PAD_W, BALL_Y_SPEED)
    # горизонтальный отскок
    if ball_top + BALL_Y_SPEED < 0 or ball_bot + BALL_Y_SPEED > HEIGHT:
        bounce("ricochet")


# Скорость ракеток
PAD_SPEED = 25

LEFT_PAD_SPEED = 0
RIGHT_PAD_SPEED = 0

# функция движения обеих ракеток
def move_pads():
    PADS = {LEFT_PAD: LEFT_PAD_SPEED,
            RIGHT_PAD: RIGHT_PAD_SPEED}
    # перебираем ракетки
    for pad in PADS:
        canvas.move(pad, 0, PADS[pad])

        if canvas.coords(pad)[1] < 0:
            canvas.move(pad, 0, -canvas.coords(pad)[1])
        elif canvas.coords(pad)[3] > HEIGHT:
            canvas.move(pad, 0, HEIGHT - canvas.coords(pad)[3])

# Логика игры компьютера
def auto_play(side, difficulty=0.3):
    global LEFT_PAD_SPEED, RIGHT_PAD_SPEED
    ball_left, ball_top, ball_right, ball_bot = canvas.coords(BALL)
    ball_center = (ball_top + ball_bot) / 2
    pad_center = (canvas.coords(side)[1] + canvas.coords(side)[3]) / 2
    if ball_center != pad_center:
        sign = (ball_center - pad_center)/abs(ball_center - pad_center)
        if abs(pad_center - ball_center) <= PAD_SPEED:
            if side == RIGHT_PAD:
                RIGHT_PAD_SPEED = sign
            elif side == LEFT_PAD:
                LEFT_PAD_SPEED = sign
        else:
            if side == RIGHT_PAD:
                RIGHT_PAD_SPEED = sign * PAD_SPEED * difficulty
            elif side == LEFT_PAD:
                LEFT_PAD_SPEED = sign * PAD_SPEED * difficulty

def main():
    move_ball()
    move_pads()
    auto_play(LEFT_PAD, 0.3)
    root.after(30, main)

# Установим фокус на Canvas чтобы он реагировал на нажатия клавиш
canvas.focus_set()

# События клавиатуры
def movement_handler(event):
    global RIGHT_PAD_SPEED
    if event.keysym == "Up":
        RIGHT_PAD_SPEED = -PAD_SPEED
    elif event.keysym == "Down":
        RIGHT_PAD_SPEED = PAD_SPEED

canvas.bind("<KeyPress>", movement_handler)

def stop_pad(event):
    global RIGHT_PAD_SPEED
    if event.keysym in ("Up", "Down"):
        RIGHT_PAD_SPEED = 0

canvas.bind("<KeyRelease>", stop_pad)

main()
root.mainloop()